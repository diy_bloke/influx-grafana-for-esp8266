#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <InfluxDb.h>

#define INFLUXDB_HOST "host_ip_nr"   //Enter IP of device running Influx Database
#define WIFI_SSID "Your SSID"              //Enter SSID of your WIFI Access Point
#define WIFI_PASS "Your Password"          //Enter Password of your WIFI Access Point

ESP8266WiFiMulti WiFiMulti;
Influxdb influx(INFLUXDB_HOST);

//BME280
Adafruit_BME280 bme; // I2C
float temperature;
float humidity;
float pressure;

// Initialize BME280
void initBME(){
  if (!bme.begin(0x76)) {
    Serial.println("No valid BME280 sensor found");
    while (1);
  }
}

void setup() {
  Serial.begin(115200);
  WiFiMulti.addAP(WIFI_SSID, WIFI_PASS);
  Serial.print("Connecting to WIFI");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  influx.setDb("garden");
  initBME();//call BME280 initiation routine
  Serial.println("Setup Complete.");
}

int recordnr = 0;

void loop() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure()/100.0F;

  loopCount++;
  InfluxData row("beds");
  
  row.addTag("Sensor", "BME280");
  row.addTag("RecordNr", String(recordno));
  row.addValue("temperature", temperature);
  row.addValue("humidity", humidity);
  row.addValue("pressure",pressure);
  row.addValue("rssi", WiFi.RSSI());
  influx.write(row);
  
 Serial.print(String(recordno));
 Serial.println(" Written to influcDB");
  
  Serial.println();
  delay(60000);
}
