#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <InfluxDb.h>

#define INFLUXDB_HOST "yourIP"   //Enter IP of device running Influx Database
#define WIFI_SSID "yourssid"              //Enter SSID of your WIFI Access Point
#define WIFI_PASS "yourpw"          //Enter Password of your WIFI Access Point

ESP8266WiFiMulti WiFiMulti;
Influxdb influx(INFLUXDB_HOST);

void setup() {
  Serial.begin(115200);
  WiFiMulti.addAP(WIFI_SSID, WIFI_PASS);
  Serial.print("Connecting to WIFI");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  influx.setDb("garden");

  Serial.println("Setup Complete.");
}



void loop() {
  
  InfluxData row("beds");
  row.addTag("bedname", "bed1");
  row.addValue("moisture", random(500,800));
  row.addValue("temperature", random(10, 40));
  row.addValue("licht", random(1100,1400));
  row.addValue("druk", random(980, 1023));
  influx.write(row);
  delay(20000);
}
